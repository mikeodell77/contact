json.array!(@phone_types) do |phone_type|
  json.extract! phone_type, :id, :name
  json.url phone_type_url(phone_type, format: :json)
end
