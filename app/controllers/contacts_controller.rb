class ContactsController < ApplicationController
  load_and_authorize_resource
  before_action :set_contact, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  
  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.where(user_id: current_user.id).order(sort_column + " " + sort_direction)
  end 

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
    1.times { @contact.addresses.build }
    1.times { @contact.phones.build }
  end

  # GET /contacts/1/edit
  def edit
    if @contact.addresses.empty?
      1.times { @contact.addresses.build }
    end
    
    if @contact.phones.empty?
      1.times { @contact.phones.build }
    end
    
    
  end

  # POST /contacts
  # POST /contacts.json
  def create
    contact_params[:user_id] = current_user
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        1.times { @contact.addresses.build }
        1.times { @contact.phones.build }
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        1.times { @contact.addresses.first }
        1.times { @contact.phones.first }        
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: "#{@contact.name} was successfully deleted." }
      format.json { head :no_content }
    end
  end

  private
  
    def sort_column
      Contact.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction])? params[:direction] : 'asc'
    end
  
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :email, :user_id, phones_attributes: [:id, :number, :phone_type_id, :_destroy], addresses_attributes: [:id, :address1, :address2, :city, :state, :zip, :_destroy])
    end
end
