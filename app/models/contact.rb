class Contact < ActiveRecord::Base
  has_many :addresses, dependent: :destroy
  has_many :phones, dependent: :destroy
  belongs_to :user
    
  validates :first_name, :user_id, presence: :true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: [:create, :update] } , allow_blank: true
  
  accepts_nested_attributes_for :addresses, allow_destroy: true, :reject_if => lambda { |a| a[:address1].blank? && a[:address2].blank? && a[:city].blank? && a[:state].blank? && a[:zip].blank? }
  accepts_nested_attributes_for :phones, allow_destroy: true, :reject_if => lambda { |a| a[:number].blank? }
  
  def name
    "#{first_name} #{last_name}"
  end
end
