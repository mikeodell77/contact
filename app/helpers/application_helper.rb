module ApplicationHelper
  def sortable(column_name, title)
    title ||= column_name.titleize
    direction = (column_name == params[:sort] && params[:direction] == "asc") ? "desc" : "asc"
    link_to title, :sort => column_name, :direction => direction
  end
end
