class AlterPhonesAddContactId < ActiveRecord::Migration
  def change
    rename_column :phones, :user_id, :contact_id
  end
end
