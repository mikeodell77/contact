class UpdatePhoneTypeInPhone < ActiveRecord::Migration
  def change
    rename_column :phones, :phone_type, :phone_type_id
  end
end
