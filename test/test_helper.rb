ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!
  # DatabaseCleaner.strategy = :truncation
  # before_setup { DatabaseCleaner.start }
  # after_teardown  { DatabaseCleaner.clean } 
  # Destroy all models because they do not get destroyed automatically

  
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end
