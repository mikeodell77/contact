require 'test_helper'

class ContactsControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  
  setup do
    @contact = contacts(:contact1)
  end

  test "should get index" do
    sign_in users(:user1)
    get :index
    assert_response :success
    assert_not_nil assigns(:contacts)
  end

  test "should get new" do
    sign_in users(:user1)
    get :new
    assert_response :success
  end

  test "should create contact" do
    sign_in users(:user1)
    assert_difference('Contact.count') do
      post :create, contact: { first_name: @contact.first_name, last_name: @contact.last_name, user_id: users(:user1).id }
    end

    assert_redirected_to contact_path(assigns(:contact))
  end

  test "should show contact" do
    sign_in users(:user1)
    get :show, id: @contact
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:user1)
    get :edit, id: @contact
    assert_response :success
  end

  test "should update contact" do
    sign_in users(:user1)
    patch :update, id: @contact, contact: { first_name: @contact.first_name, last_name: @contact.last_name }
    assert_redirected_to contact_path(assigns(:contact))
  end

  test "should destroy contact" do
    sign_in users(:user1)
    assert_difference('Contact.count', -1) do
      delete :destroy, id: @contact
    end

    assert_redirected_to contacts_path
  end
end
