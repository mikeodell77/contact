require 'test_helper'


class PhoneTypesControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    @phone_type = phone_types(:phone_type1)
  end

  test "should get index" do
    sign_in users(:user1)
    get :index
    assert_response :success
    assert_not_nil assigns(:phone_types)
  end

  test "should get new" do
    sign_in users(:user1)
    get :new
    assert_response :success
  end

  test "should create phone_type" do
    sign_in users(:user1)
    assert_difference('PhoneType.count') do
      post :create, phone_type: { name: @phone_type.name }
    end

    assert_redirected_to phone_type_path(assigns(:phone_type))
  end

  test "should show phone_type" do
    sign_in users(:user1)
    get :show, id: @phone_type
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:user1)
    get :edit, id: @phone_type
    assert_response :success
  end

  test "should update phone_type" do
    sign_in users(:user1)
    patch :update, id: @phone_type, phone_type: { name: @phone_type.name }
    assert_redirected_to phone_type_path(assigns(:phone_type))
  end

  test "should destroy phone_type" do
    sign_in users(:user1)
    assert_difference('PhoneType.count', -1) do
      delete :destroy, id: @phone_type
    end

    assert_redirected_to phone_types_path
  end
end
