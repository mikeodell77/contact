require 'test_helper'

class PhoneTypeTest < ActiveSupport::TestCase
  
  setup do
    @phone_type = phone_types(:phone_type1)
  end
  
  test "should not save phone type without name" do
    phone_type = PhoneType.new()
    assert_not phone_type.save, "Should not save a phone type without a name"
  end
  
  test "should save with name" do
    phone_type = PhoneType.new(name: @phone_type.name)
    assert phone_type.save, "We should be saving a phone type"
  end
end
