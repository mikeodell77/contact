require 'test_helper'

class PhoneTest < ActiveSupport::TestCase
 
  setup do
    @phone = phones(:phone1)
  end
  
  test "should save" do
    phone = Phone.new(number: @phone.number, phone_type_id: @phone.phone_type_id, contact_id: @phone.contact_id)
    assert phone.save, "Phone should be saved"
  end
  
end
