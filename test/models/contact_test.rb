require 'test_helper'

class ContactTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @contact = contacts(:contact1)
  end
  
  test "should not create without first name" do
    contact = Contact.new()
    assert_not contact.save, "We shouldn't be saving witout a first name"
  end
  
  test "sould create a contact" do
    contact = Contact.new(first_name: @contact.first_name, last_name: @contact.last_name, user_id: @contact.user_id)
    assert contact.save, "Contact should have been saved"
  end
  
  test "should not create without a user_id" do
    contact = Contact.new(first_name: @contact.first_name, last_name: @contact.last_name)
    assert_not contact.save, "We shouldn't be saving without a user_id"
  end
  
  test "should return name of contact" do
    contact = Contact.new(first_name: @contact.first_name, last_name: @contact.last_name, user_id: @contact.user_id)
    assert_equal contact.name, "#{@contact.first_name} #{@contact.last_name}", "Contact name is not correct"
  end
  
  test "should return name of contact even with just first name" do
    contact = Contact.new(first_name: @contact.first_name, last_name: @contact.last_name, user_id: @contact.user_id)
    assert_equal contact.name, "#{@contact.first_name} #{@contact.last_name}", "Contact name is not correct"
  end
end
