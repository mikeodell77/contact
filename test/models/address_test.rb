require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  setup do
    @address = addresses(:address1)
  end
  
  test "should save with minimal data" do
    address = Address.new(address1: @address.address1, city: @address.city, zip: @address.zip)
    assert address.save, "Should create an address"
  end
  
  test "should save" do
    address = Address.new(address1: @address.address1, city: @address.city, state: @address.state, zip: @address.zip, contact_id: @address.contact_id)
    assert address.save
  end
end
